use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::env;

use serenity::async_trait;
use serenity::model::application::command::{Command, CommandOptionType};
use serenity::model::application::interaction::application_command::CommandDataOptionValue;
use serenity::model::application::interaction::{Interaction, InteractionResponseType};
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::prelude::*;

const TOCFL1: &str = include_str!("/tmp/tocfl1.json");
const TOCFL2: &str = include_str!("/tmp/tocfl2.json");
const TOCFL3: &str = include_str!("/tmp/tocfl3.json");
const TOCFL4: &str = include_str!("/tmp/tocfl4.json");
const TOCFL5: &str = include_str!("/tmp/tocfl5.json");
const TOCFL6: &str = include_str!("/tmp/tocfl6.json");
const TOCFL7: &str = include_str!("/tmp/tocfl7.json");

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq)]
struct TocflWord {
    chs: String,
    cht: String,
    pinyin: Vec<String>,
    zhuyin: String,
    translation: Vec<String>,
}

// struct Handler;
//
// #[async_trait]
// impl EventHandler for Handler {
//     async fn message(&self, ctx: Context, msg: Message) {
//         if msg.content == "!hello" {
//             // The create message builder allows you to easily create embeds and messages
//             // using a builder syntax.
//             // This example will create a message that says "Hello, World!", with an embed that has
//             // a title, description, an image, three fields, and a footer.
//             let msg = msg
//                 .channel_id
//                 .send_message(&ctx.http, |m| {
//                     m.content("Hello, World!")
//                         .embed(|e| {
//                             e.title("This is a title")
//                                 .description("This is a description")
//                                 .image("attachment://ferris_eyes.png")
//                                 .fields(vec![
//                                     ("This is the first field", "This is a field body", true),
//                                     ("This is the second field", "Both fields are inline", true),
//                                 ])
//                                 .field(
//                                     "This is the third field",
//                                     "This is not an inline field",
//                                     false,
//                                 )
//                                 .footer(|f| f.text("This is a footer"))
//                                 // Add a timestamp for the current time
//                                 // This also accepts a rfc3339 Timestamp
//                                 .timestamp(Timestamp::now())
//                         })
//                         .add_file("./ferris_eyes.png")
//                 })
//                 .await;
//
//             if let Err(why) = msg {
//                 println!("Error sending message: {:?}", why);
//             }
//         }
//     }
//
//     async fn ready(&self, _: Context, ready: Ready) {
//         println!("{} is connected!", ready.user.name);
//     }
// }
//

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn interaction_create(&self, ctx: Context, interaction: Interaction) {
        if let Interaction::ApplicationCommand(command) = interaction {
            println!("Received command interaction: {:#?}", command);

            let content = match command.data.name.as_str() {
                "tocfl" => {
                    let options: Vec<u8> = command
                        .data
                        .options
                        .iter()
                        .map(|o| {
                            let resolved =
                                o.clone().resolved.expect("Failed to get data from options");
                            if let CommandDataOptionValue::Integer(int) = resolved {
                                int as u8
                            } else {
                                panic!("Failed!");
                            }
                        })
                        .collect();
                    format!("{:#?}", options)
                }
                "ping" => "Hey, I'm alive!".to_string(),
                "id" => {
                    let options = command
                        .data
                        .options
                        .get(0)
                        .expect("Expected user option")
                        .resolved
                        .as_ref()
                        .expect("Expected user object");

                    if let CommandDataOptionValue::User(user, _member) = options {
                        format!("{}'s id is {}", user.tag(), user.id)
                    } else {
                        "Please provide a valid user".to_string()
                    }
                }
                "attachmentinput" => {
                    let options = command
                        .data
                        .options
                        .get(0)
                        .expect("Expected attachment option")
                        .resolved
                        .as_ref()
                        .expect("Expected attachment object");

                    if let CommandDataOptionValue::Attachment(attachment) = options {
                        format!(
                            "Attachment name: {}, attachment size: {}",
                            attachment.filename, attachment.size
                        )
                    } else {
                        "Please provide a valid attachment".to_string()
                    }
                }
                _ => "not implemented :(".to_string(),
            };

            if let Err(why) = command
                .create_interaction_response(&ctx.http, |response| {
                    response
                        .kind(InteractionResponseType::ChannelMessageWithSource)
                        .interaction_response_data(|message| message.content(content))
                })
                .await
            {
                println!("Cannot respond to slash command: {}", why);
            }
        }
    }

    async fn ready(&self, ctx: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);

        let command = Command::create_global_application_command(&ctx.http, |command| {
            // let commands = GuildId::set_application_commands(&guild_id, &ctx.http, |command| {
            // commands.create_application_command(|command| {
            command
                .name("tocfl")
                .description("Study TOCFL vocabulary")
                .create_option(|option| {
                    option
                        .name("level")
                        .description("TOCFL band level")
                        .kind(CommandOptionType::Integer)
                        .min_int_value(1u8)
                        .max_int_value(7u8)
                        .required(true)
                })
                .create_option(|option| {
                    option
                        .name("rounds")
                        .description("How many rounds to play.")
                        .kind(CommandOptionType::Integer)
                        .required(true)
                })
                .create_option(|option| {
                    option
                        .name("delay")
                        .description("The timeout for each round.")
                        .kind(CommandOptionType::Integer)
                        .required(true)
                })
        })
        .await;

        println!(
            "I created the following global slash command: {:#?}",
            command
        );
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");

    let tocfl1: Vec<TocflWord> = serde_json::from_str(TOCFL1)?;
    let tocfl2: Vec<TocflWord> = serde_json::from_str(TOCFL2)?;
    let tocfl3: Vec<TocflWord> = serde_json::from_str(TOCFL3)?;
    let tocfl4: Vec<TocflWord> = serde_json::from_str(TOCFL4)?;
    let tocfl5: Vec<TocflWord> = serde_json::from_str(TOCFL5)?;
    let tocfl6: Vec<TocflWord> = serde_json::from_str(TOCFL6)?;
    let tocfl7: Vec<TocflWord> = serde_json::from_str(TOCFL7)?;

    // Build our client.
    let intents = GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::DIRECT_MESSAGES
        | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(&token, intents)
        .event_handler(Handler)
        .await
        .expect("Err creating client");

    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }

    Ok(())
}
